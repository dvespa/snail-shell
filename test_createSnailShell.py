from createSnailShell import createSnailShell


def test_createSnailShell():
    # Test Case 1: N = 0
    assert createSnailShell(0) == []
    print("Test 1 OK")

    # Test Case 2: N = 1
    assert createSnailShell(1) == [[1]]
    print("Test 2 OK")

    # Test Case 3: N = 2
    assert createSnailShell(2) == [[1, 2], [4, 3]]
    print("Test 3 OK")

    # Test Case 4: N = 3
    assert createSnailShell(3) == [[1, 2, 3], [8, 9, 4], [7, 6, 5]]
    print("Test 4 OK")

    # Test Case 5: N = 4
    assert createSnailShell(4) == [[1, 2, 3, 4], [12, 13, 14, 5], [
        11, 16, 15, 6], [10, 9, 8, 7]]
    print("Test 5 OK")

    # Test Case 6: N = 5
    assert createSnailShell(5) == [[1, 2, 3, 4, 5], [16, 17, 18, 19, 6], [
        15, 24, 25, 20, 7], [14, 23, 22, 21, 8,], [13, 12, 11, 10, 9]]
    print("Test 6 OK")

    # Test Case 7: N is not an integer
    assert createSnailShell(1.414) == []
    print("Test 7 OK")


test_createSnailShell()
