def print_matrix(matrix):
    for row in matrix:
        for element in row:
            print(element, end=' ')
        print()


def createSnailShell(x):
    result = []
    if (type(x) != int or x < 1):
        print(result)
        return result
    num, pos = 1, 0
    for i in range(x):
        r = []
        for _ in range(x):
            r.append(0)
        result.append(r)
    while num <= x * x:
        for i in range(pos, x - pos):
            result[pos][i] = num
            num += 1
        for i in range(pos + 1, x - pos):
            result[i][x - pos - 1] = num
            num += 1
        for i in range(x - pos - 2, pos - 1, -1):
            result[x - pos - 1][i] = num
            num += 1
        for i in range(x - pos - 2, pos, -1):
            result[i][pos] = num
            num += 1
        pos += 1
    print_matrix(result)
    return result
