#Snail-Shell
## Table of contents
* [General info](#general-info)
* [Setup](#setup)

## General info
This project is a simple take home test. Given a parameter x it returns an NxN two dimensional array with numbers ranging from 1 through x^2 that are shown in a format that resembles a snail shell.
	
## Setup
To run this project, clone this repository and run the main file:

```
$ cd ../snail-shell
$ python3 main.py
```

To run the tests you'll need to run ```test_createSnailShell.py```
